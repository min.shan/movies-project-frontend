function isRequired() {
  return function(value) {
    return !!value || `Cannot be empty`;
  };
}

function isStringShorterThan(maxLength) {
  return function(string) {
    return (
      (!!string && string.length <= maxLength) ||
      `Must be shorter than ${maxLength} characters`
    );
  };
}

function isInt() {
  return function(value) {
    return (
      (!!parseInt(value) && parseInt(value, 10)) || `Must be a whole number`
    );
  };
}

function isBetween(minimum, maximum) {
  return function(value) {
    return (
      (parseFloat(value) &&
        parseFloat(value) >= minimum &&
        parseFloat(value) <= maximum) ||
      `Value must be between $${minimum} and $${maximum}`
    );
  };
}

module.exports = {
  isBetween,
  isInt,
  isRequired,
  isStringShorterThan
};
