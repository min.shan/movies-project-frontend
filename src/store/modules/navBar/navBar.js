export default {
  namespaced: true,
  state() {
    return {
      showDrawer: false
    };
  },
  getters: {
    getShowDrawer(state) {
      return state.showDrawer;
    }
  },
  mutations: {
    setShowDrawer(state, showDrawer) {
      state.showDrawer = showDrawer;
    }
  },
  actions: {
    openDrawer({ commit }) {
      commit("setShowDrawer", true);
    },
    closeDrawer({ commit }) {
      commit("setShowDrawer", false);
    },
    toggleDrawer({ commit, state }) {
      commit("setShowDrawer", !state.showDrawer);
    }
  }
};
