const state = {
  message: ""
};

const getters = {
  getSnackbarMessage(state) {
    return state.message;
  }
};

const mutations = {
  setSnackMessage(state, snackMessage) {
    state.message = snackMessage;
  }
};

const namespaced = true;

module.exports = {
  getters,
  mutations,
  namespaced,
  state
};
