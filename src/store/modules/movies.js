import moviesService from "../../service/moviesService";

const namespaced = true;

const state = {
  list: [],
  modifiedMovie: null
};

const getters = {
  getAllMovies: state => state.list,
  getModifiedMovie: state => state.modifiedMovie,
  getMovieById: state => id =>
    state.list.find(movie => movie.id === parseInt(id))
};

const mutations = {
  setMovies(state, payload) {
    state.list = payload.movies;
  },
  setModifiedMovie(state, payload) {
    state.modifiedMovie = payload;
  }
};

const actions = {
  async findAll({ commit }) {
    const movies = await moviesService.findAllMovies();
    commit("setMovies", movies.data);
  },
  async insert({ commit, dispatch }, movie) {
    await moviesService.insertMovie(movie);
    await dispatch("findAll");
    commit("setModifiedMovie", { ...movie, changeType: "inserted" });
  },
  async delete({ commit, dispatch }, movie) {
    await moviesService.deleteMovie(movie.id);
    await dispatch("findAll");
    commit("setModifiedMovie", { ...movie, changeType: "deleted" });
  }
};

export default {
  actions,
  getters,
  mutations,
  namespaced,
  state
};
