import Vue from "vue";
import Vuex from "vuex";
import movies from "./modules/movies";
import snackbar from "./modules/snackbar";
import navBar from "./modules/navBar";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    movies,
    snackbar,
    navBar
  }
});
