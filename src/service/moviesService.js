import axios from "axios";

function findAllMovies() {
  return axios.get("http://localhost:3000/movies");
}

function insertMovie(movieData) {
  return axios.post("http://localhost:3000/movies", movieData);
}

function deleteMovie(id) {
  return axios.delete(`http://localhost:3000/movies/${id}`);
}

export default {
  deleteMovie,
  findAllMovies,
  insertMovie
};
