import Vue from "vue";
import Router from "vue-router";
import MoviesList from "./views/MoviesList.vue";
import Movie from "./views/Movie.vue";
Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    { path: "/", redirect: "/movies" },
    {
      path: "/movies",
      name: "moviesList",
      component: MoviesList,
      props: true
    },
    {
      path: "/movies/:id",
      name: "movie",
      component: Movie
    }
  ]
});
